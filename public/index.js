const checkoutHandler = StripeCheckout.configure({
    key: "pk_test_QpHtrUx2wKISWjDBfXSvzBZk00goZ3e9kD",
    locale: "auto"
});

const button = document.getElementById("buttonCheckout");
button.addEventListener("click", ev => {
    checkoutHandler.open({
        name: "Sample Store",
        description: "Example Purchase",
        token: handleToken
    });
});

handleToken = token => {
    fetch("/charge", {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(token)
    })
    .then(response => {
        if (!response.ok)
            throw response;
        return response.json();
    })
    .then(output => {
        console.log("Purchase succeeded:", output);
    })
    .catch(err => {
        console.log("Purchase failed:", err);
    });
}