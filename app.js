const config = require('./.dev.env.js');
const express = require('express');
const port = 5000 || 5001
const app = express();
const keySecret = config.STRIPE_SECRET;
const stripe = require("stripe")(keySecret);
const bodyParser = require("body-parser");


app.use(express.static("public"));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.post("/charge", (req, res) => {
    // value in cents
    let amount = 1000;
    /**
     * Creating the Customer object is optional, 
     * but it allows you to perform future charges 
     * for the user without collecting their credit 
     * card information every time.
     */
    stripe.customers.create({
      email: req.body.email,
      card: req.body.id
    })
    .then(customer =>
      stripe.charges.create({
        amount,
        description: "Sample Charge",
        currency: "eur",
        customer: customer.id
      }))
    .then(charge => res.send(charge))
    .catch(err => {
      console.log("Error:", err);
      res.status(500).send({error: "Purchase Failed"});
    });
  });



app.listen(port, ()=> {
    console.log(`Server listening on port ${port}`);
});